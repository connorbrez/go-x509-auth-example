# Mutual x509 authentication server & client written in go.

- key size is 4096 for this example.

##### The case for using 2048 bits instead of 4096 bits
- Some hardware (many smart cards, some card readers, and some other devices such as Polycom phones) don't support anything bigger than 2048 bits.
- Uses less CPU than a longer key during encryption and authentication
- Using less CPU means using less battery drain (important for mobile devices)
- Uses less storage space: while not an issue on disk, this can be an issue in small devices like smart cards that measure their RAM in kilobytes rather than gigabytes

## Certificate Authority(CA) Key & Cert

#### generate CA key
`openssl genrsa -out ca.key 4096`  

#### generate CA cert with CA key
`openssl req -new -x509 -days 365 -key ca.key -out ca.crt`

## Server Key & Certificate Signing Request (CSR)

#### generate server.key
`openssl genrsa -out server.key 4096`

#### generate CSR with server.key
`openssl req -new -key server.key -out server.csr`

#### sign server.csr with CA key & cert
`openssl x509 -req -days 365 -in server.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out server.crt`

## Client Key & Certificate Signing Request (CSR)

#### generate client.key
`openssl genrsa -out client.key 4096`

#### generate CSR with client.key
`openssl req -new -key client.key -out client.csr`

#### sign client.csr with CA key & cert
`openssl x509 -req -days 365 -in client.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out client.crt`

##### note: 
the -des3 flag was removed to generate keys without DES encryption. If enabled, you will be prompted for passphrase and key will be encrypted. Every server startup with a Certificate using a DES encrypted key, you will be prompted for that password.

## useful random links & references

https://danielpocock.com/rsa-key-sizes-2048-or-4096-bits  
https://serverfault.com/questions/366372/is-it-possible-to-generate-rsa-key-without-pass-phrase  
https://github.com/jomoespe/go-tls-mutual-auth  
https://medium.com/@itseranga/tls-mutual-authentication-with-golang-and-nginx-937f0da22a0e  