package main

import (
	"crypto/tls"
	"crypto/x509"
	"io/ioutil"
	"log"
	"net/http"

	"golang.org/x/net/http2"
)

func main() {
	log.SetFlags(log.Lshortfile)

	http.HandleFunc("/hello", HelloServer)

	// get root(CA) cert
	caCert, err := ioutil.ReadFile("../certs/ca.crt")
	if err != nil {
		log.Fatal(err)
	}
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)

	// add root CAs & require a verified client certificate to connect
	tlsConfig := &tls.Config{
		ClientCAs:  caCertPool,
		ClientAuth: tls.RequireAndVerifyClientCert,
	}
	tlsConfig.BuildNameToCertificate()

	// configure & init http server
	server := &http.Server{
		Addr:      ":8443",
		TLSConfig: tlsConfig,
	}
	http2.ConfigureServer(server, nil)

	err = server.ListenAndServeTLS("../certs/server.crt", "../certs/server.key")
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func HelloServer(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte("This is an example server.\n"))
}
